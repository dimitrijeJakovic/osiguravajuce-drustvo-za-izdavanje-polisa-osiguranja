# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Projektni rad na fakultetu                                                                                                             2017
Kompletan informacioni sistem za Osiguravajuce drustvo za izdavanje polisa osiguranja (MVC, PHP + MySql) koji se 
sastoji od admin dela, gde se nalaze opcije za unos, izmenu, aktivaciju i deaktivaciju i brisanje studenata, 
kao i polje za pretragu studenata po imenu i JMBG-u. Zatim, tu se nalaze i opcije za pravljenje novih polisa, 
izmenu starih, brisanje, aktivaciju i deaktivaciju postojacih, potom dugme za stampanje polisa, polje za pretragu 
polisa po vrsti polise, imenu polise i JMBG-u za koga je ta polisa. Uz to sve, nalazi se i panel  za korisnike gde 
korisnici mogu da izmene svoje podatke, pogledaju i stampaju svoje vec kreirane polise. Kompletan informacioni 
sistem je prilagodjiv razlicitim velicinama ekrana


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact