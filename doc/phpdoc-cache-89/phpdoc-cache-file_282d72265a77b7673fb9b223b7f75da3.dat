O:39:"phpDocumentor\Descriptor\FileDescriptor":22:{s:7:" * hash";s:32:"d04235e791b8d2b5f871eb03a3685ccb";s:7:" * path";s:56:"vendor\symfony\var-dumper\Tests\Cloner\VarClonerTest.php";s:9:" * source";s:7534:"<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\VarDumper\Tests\Cloner;

use PHPUnit\Framework\TestCase;
use Symfony\Component\VarDumper\Cloner\VarCloner;

/**
 * @author Nicolas Grekas <p@tchwork.com>
 */
class VarClonerTest extends TestCase
{
    public function testMaxIntBoundary()
    {
        $data = array(PHP_INT_MAX => 123);

        $cloner = new VarCloner();
        $clone = $cloner->cloneVar($data);

        $expected = <<<EOTXT
Symfony\Component\VarDumper\Cloner\Data Object
(
    [data:Symfony\Component\VarDumper\Cloner\Data:private] => Array
        (
            [0] => Array
                (
                    [0] => Array
                        (
                            [1] => 1
                        )

                )

            [1] => Array
                (
                    [%s] => 123
                )

        )

    [position:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [key:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [maxDepth:Symfony\Component\VarDumper\Cloner\Data:private] => 20
    [maxItemsPerDepth:Symfony\Component\VarDumper\Cloner\Data:private] => -1
    [useRefHandles:Symfony\Component\VarDumper\Cloner\Data:private] => -1
)

EOTXT;
        $this->assertSame(sprintf($expected, PHP_INT_MAX), print_r($clone, true));
    }

    public function testClone()
    {
        $json = json_decode('{"1":{"var":"val"},"2":{"var":"val"}}');

        $cloner = new VarCloner();
        $clone = $cloner->cloneVar($json);

        $expected = <<<EOTXT
Symfony\Component\VarDumper\Cloner\Data Object
(
    [data:Symfony\Component\VarDumper\Cloner\Data:private] => Array
        (
            [0] => Array
                (
                    [0] => Symfony\Component\VarDumper\Cloner\Stub Object
                        (
                            [type] => 4
                            [class] => stdClass
                            [value] => 
                            [cut] => 0
                            [handle] => %i
                            [refCount] => 0
                            [position] => 1
                            [attr] => Array
                                (
                                )

                        )

                )

            [1] => Array
                (
                    [\000+\0001] => Symfony\Component\VarDumper\Cloner\Stub Object
                        (
                            [type] => 4
                            [class] => stdClass
                            [value] => 
                            [cut] => 0
                            [handle] => %i
                            [refCount] => 0
                            [position] => 2
                            [attr] => Array
                                (
                                )

                        )

                    [\000+\0002] => Symfony\Component\VarDumper\Cloner\Stub Object
                        (
                            [type] => 4
                            [class] => stdClass
                            [value] => 
                            [cut] => 0
                            [handle] => %i
                            [refCount] => 0
                            [position] => 3
                            [attr] => Array
                                (
                                )

                        )

                )

            [2] => Array
                (
                    [\000+\000var] => val
                )

            [3] => Array
                (
                    [\000+\000var] => val
                )

        )

    [position:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [key:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [maxDepth:Symfony\Component\VarDumper\Cloner\Data:private] => 20
    [maxItemsPerDepth:Symfony\Component\VarDumper\Cloner\Data:private] => -1
    [useRefHandles:Symfony\Component\VarDumper\Cloner\Data:private] => -1
)

EOTXT;
        $this->assertStringMatchesFormat($expected, print_r($clone, true));
    }

    public function testJsonCast()
    {
        if (ini_get('xdebug.overload_var_dump') == 2) {
            $this->markTestSkipped('xdebug is active');
        }

        $data = (array) json_decode('{"1":{}}');

        $cloner = new VarCloner();
        $clone = $cloner->cloneVar($data);

        $expected = <<<'EOTXT'
object(Symfony\Component\VarDumper\Cloner\Data)#%i (6) {
  ["data":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  array(2) {
    [0]=>
    array(1) {
      [0]=>
      array(1) {
        [1]=>
        int(1)
      }
    }
    [1]=>
    array(1) {
      ["1"]=>
      object(Symfony\Component\VarDumper\Cloner\Stub)#%i (8) {
        ["type"]=>
        int(4)
        ["class"]=>
        string(8) "stdClass"
        ["value"]=>
        NULL
        ["cut"]=>
        int(0)
        ["handle"]=>
        int(%i)
        ["refCount"]=>
        int(0)
        ["position"]=>
        int(0)
        ["attr"]=>
        array(0) {
        }
      }
    }
  }
  ["position":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  int(0)
  ["key":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  int(0)
  ["maxDepth":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  int(20)
  ["maxItemsPerDepth":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  int(-1)
  ["useRefHandles":"Symfony\Component\VarDumper\Cloner\Data":private]=>
  int(-1)
}

EOTXT;
        ob_start();
        var_dump($clone);
        $this->assertStringMatchesFormat(\PHP_VERSION_ID >= 70200 ? str_replace('"1"', '1', $expected) : $expected, ob_get_clean());
    }

    public function testCaster()
    {
        $cloner = new VarCloner(array(
            '*' => function ($obj, $array) {
                return array('foo' => 123);
            },
            __CLASS__ => function ($obj, $array) {
                ++$array['foo'];

                return $array;
            },
        ));
        $clone = $cloner->cloneVar($this);

        $expected = <<<EOTXT
Symfony\Component\VarDumper\Cloner\Data Object
(
    [data:Symfony\Component\VarDumper\Cloner\Data:private] => Array
        (
            [0] => Array
                (
                    [0] => Symfony\Component\VarDumper\Cloner\Stub Object
                        (
                            [type] => 4
                            [class] => %s
                            [value] => 
                            [cut] => 0
                            [handle] => %i
                            [refCount] => 0
                            [position] => 1
                            [attr] => Array
                                (
                                )

                        )

                )

            [1] => Array
                (
                    [foo] => 124
                )

        )

    [position:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [key:Symfony\Component\VarDumper\Cloner\Data:private] => 0
    [maxDepth:Symfony\Component\VarDumper\Cloner\Data:private] => 20
    [maxItemsPerDepth:Symfony\Component\VarDumper\Cloner\Data:private] => -1
    [useRefHandles:Symfony\Component\VarDumper\Cloner\Data:private] => -1
)

EOTXT;
        $this->assertStringMatchesFormat($expected, print_r($clone, true));
    }
}
";s:19:" * namespaceAliases";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:8:"TestCase";s:27:"\PHPUnit\Framework\TestCase";s:9:"VarCloner";s:45:"\Symfony\Component\VarDumper\Cloner\VarCloner";}}s:11:" * includes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * functions";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * classes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:55:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest";O:40:"phpDocumentor\Descriptor\ClassDescriptor":19:{s:9:" * parent";s:27:"\PHPUnit\Framework\TestCase";s:13:" * implements";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:11:" * abstract";b:0;s:8:" * final";b:0;s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:13:" * properties";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * methods";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:4:{s:18:"testMaxIntBoundary";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:17;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:77:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest::testMaxIntBoundary()";s:7:" * name";s:18:"testMaxIntBoundary";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:22;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50008";s:7:" * line";i:22;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:20:"testMaxIntBoundary()";}}}}s:19:" * inheritedElement";N;}s:9:"testClone";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:17;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:68:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest::testClone()";s:7:" * name";s:9:"testClone";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:61;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50008";s:7:" * line";i:61;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:11:"testClone()";}}}}s:19:" * inheritedElement";N;}s:12:"testJsonCast";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:17;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:71:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest::testJsonCast()";s:7:" * name";s:12:"testJsonCast";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:149;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50008";s:7:" * line";i:149;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:14:"testJsonCast()";}}}}s:19:" * inheritedElement";N;}s:10:"testCaster";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:17;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:69:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest::testCaster()";s:7:" * name";s:10:"testCaster";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:214;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50008";s:7:" * line";i:214;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:12:"testCaster()";}}}}s:19:" * inheritedElement";N;}}}s:13:" * usedTraits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:55:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest";s:7:" * name";s:13:"VarClonerTest";s:12:" * namespace";s:41:"\Symfony\Component\VarDumper\Tests\Cloner";s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";r:1;s:7:" * line";i:20;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:3:{s:6:"author";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\AuthorDescriptor":3:{s:7:" * name";s:6:"author";s:14:" * description";s:30:"Nicolas Grekas <p@tchwork.com>";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:7:"package";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"package";s:14:" * description";s:7:"Default";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50005";s:7:" * line";i:20;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:55:"\Symfony\Component\VarDumper\Tests\Cloner\VarClonerTest";}}}}s:19:" * inheritedElement";N;}}}s:13:" * interfaces";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * traits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * markers";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:0:"";s:7:" * name";s:17:"VarClonerTest.php";s:12:" * namespace";N;s:10:" * package";s:7:"Default";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"package";r:176;s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50000";s:7:" * line";i:0;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:0:"";}}}}s:19:" * inheritedElement";N;}