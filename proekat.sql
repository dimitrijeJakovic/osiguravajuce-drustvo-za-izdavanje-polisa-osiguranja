-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2017 at 05:45 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proekat`
--

-- --------------------------------------------------------

--
-- Table structure for table `police_assets`
--

CREATE TABLE `police_assets` (
  `police_assets_id` int(10) NOT NULL,
  `user_police_id` int(10) NOT NULL,
  `police_type_id` int(10) NOT NULL,
  `police_meta_id` int(10) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `police_assets`
--

INSERT INTO `police_assets` (`police_assets_id`, `user_police_id`, `police_type_id`, `police_meta_id`, `value`) VALUES
(102, 41, 6, 19, 'PO123AA'),
(103, 41, 6, 20, '111111111111'),
(104, 41, 6, 21, '111111111111'),
(105, 41, 6, 22, 'Volcvagena'),
(106, 41, 6, 23, 'Golf2'),
(107, 41, 6, 24, '2002-02-02'),
(108, 41, 6, 25, 'Sivaa'),
(116, 43, 7, 26, 'Gricica Milenka 23a'),
(117, 43, 7, 27, '150'),
(118, 43, 7, 28, '2017-01-01'),
(119, 43, 7, 29, '1000'),
(120, 44, 8, 30, 'gfdghf'),
(121, 44, 8, 31, 'gjhjhhj');

-- --------------------------------------------------------

--
-- Table structure for table `police_meta`
--

CREATE TABLE `police_meta` (
  `police_meta_id` int(10) NOT NULL,
  `police_type_id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `type` text NOT NULL,
  `active` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `police_meta`
--

INSERT INTO `police_meta` (`police_meta_id`, `police_type_id`, `name`, `type`, `active`) VALUES
(19, 6, 'Registracija', 'text', 1),
(20, 6, 'Broj Sasije', 'text', 1),
(21, 6, 'Broj Motora', 'text', 1),
(22, 6, 'Proizvodjac', 'text', 1),
(23, 6, 'Model', 'text', 1),
(24, 6, 'Datum Registracije', 'date', 1),
(25, 6, 'Boja Vozila', 'text', 1),
(26, 7, 'Adresa Nekretnine', 'text', 1),
(27, 7, 'Povrsina Nekretnine', 'text', 1),
(28, 7, 'Datum Izgradnje Nekretnine', 'date', 1),
(29, 7, 'Procenat Rizika', 'text', 1),
(30, 8, 'Tranzitne Drzve', 'text', 1),
(31, 8, 'Drzava Boravka', 'text', 1),
(32, 9, 'Mesto Slucaja', 'text', 1),
(33, 9, 'Vreme Slucaja', 'date', 1),
(34, 9, 'Mesto Slucaja', 'text', 1),
(35, 9, 'Opis Dogadjaja', 'textarea', 1),
(36, 9, 'Opis Povreda', 'textarea', 1),
(37, 9, 'Ime Przime Lekara', 'text', 1),
(38, 10, 'Izvori Finansija', 'dropdown', 1),
(39, 10, 'Naziv Programa', 'text', 1);

-- --------------------------------------------------------

--
-- Table structure for table `police_type`
--

CREATE TABLE `police_type` (
  `police_type_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `police_type`
--

INSERT INTO `police_type` (`police_type_id`, `name`) VALUES
(6, 'Auto odgovornosti'),
(7, 'Osiguranje nepokretne imovine od pozara'),
(8, 'Putno osiguranje'),
(9, 'Osiguranje od posledic anesrecnih slucajeva'),
(10, 'Osiguranje useva i plodova');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` char(128) NOT NULL,
  `forname` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `jmbg` bigint(13) NOT NULL,
  `telfon_number` text NOT NULL,
  `date_of_birth` date NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - not active, 1 - active',
  `type` varchar(1) NOT NULL DEFAULT '0' COMMENT '0 - user, 1 - worker'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `password`, `forname`, `surname`, `jmbg`, `telfon_number`, `date_of_birth`, `active`, `type`) VALUES
(5, 'dika@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Dimitrije', 'Jankovic', 1711994710130, '0601479990', '1994-11-17', 1, '0'),
(6, 'uros@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Uros', 'Radojcic', 17119947101304, '+38162125812', '1992-07-02', 1, '1'),
(8, 'nikola@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Nikola ', 'Jakovlevic', 1139942101305, '+38115001532', '2017-08-06', 0, '0'),
(9, 'mira@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Mirijana', 'Krstic', 2134234237423, '+3214234234', '2017-12-07', 1, '0'),
(10, 'nina@gamil.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Nina', 'Marica', 3424234279012, '+381054145', '2017-08-30', 1, '0'),
(45, 'test@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'dsada', 'asda', 2212321445454, '123456', '2017-08-17', 1, '0'),
(47, 'svab6a@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Uross6', 'test6', 7119947101306, '534534536', '2017-01-18', 1, '0'),
(48, 'm@gamail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'm', 'm', 10069907801305, '242342343', '2017-07-12', 1, '0'),
(49, 'm@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'm', 'm', 4343994710130, '32423423', '2000-01-01', 0, '0'),
(50, 'a@gamail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'a', 'a', 17119947178965, '3423423423', '2017-07-07', 1, '0'),
(107, 'asdasd@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'sadasd', 'dasdasd', 14129847101305, '70807080', '2017-08-11', 1, '0'),
(108, 'dasdas@gamail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'sdasdasda', 'dasdasdas', 7842210130575, '4565656', '2017-08-06', 0, '0'),
(119, 'test@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'test', 'test', 17119947101307, '1223213213', '2017-01-01', 1, '0'),
(120, 'test@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'test', 'test', 171199447101304, '3423423423', '2017-01-01', 1, '0'),
(122, 'maja@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Marija', 'Vitic', 3123213213213, '031313515', '2017-01-01', 1, '0'),
(123, 'test@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'test', 'test', 1712994710130, '3423423423', '2017-01-01', 0, '0'),
(124, 'dada@gmail.com', '962e96aa25f021152e1e68b90e074bfbb9a78b99adb419215237049ac3f2c77193df8cb945e1e399f7d7fa9c9cedb1b7f14463b09f6b3b211988295eb88ea33f', 'Dragana', 'Lavrnja', 4455546456464, '45466646464', '2017-01-01', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_police`
--

CREATE TABLE `user_police` (
  `user_police_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `police_type_id` int(10) NOT NULL,
  `start_date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `value` float(15,2) NOT NULL,
  `terms` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_police`
--

INSERT INTO `user_police` (`user_police_id`, `user_id`, `police_type_id`, `start_date`, `expire_date`, `value`, `terms`, `active`) VALUES
(41, 5, 6, '2018-01-01 00:00:00', '2020-01-01 00:00:00', 1000.00, 'aaaaaaaaaaaaaaaaaaaaaaaa', 1),
(43, 8, 7, '2017-01-01 00:00:00', '2018-01-01 00:00:00', 7777.00, 'jkjhhj', 1),
(44, 50, 8, '2017-01-01 00:00:00', '2017-01-01 00:00:00', 555.00, '34', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `police_assets`
--
ALTER TABLE `police_assets`
  ADD PRIMARY KEY (`police_assets_id`),
  ADD KEY `FK_police_assets_police_type` (`police_type_id`),
  ADD KEY `FK_police_assets_police_meta` (`police_meta_id`),
  ADD KEY `FK_police_assets_user_police` (`user_police_id`);

--
-- Indexes for table `police_meta`
--
ALTER TABLE `police_meta`
  ADD PRIMARY KEY (`police_meta_id`),
  ADD KEY `FK_police_meta_police_type` (`police_type_id`);

--
-- Indexes for table `police_type`
--
ALTER TABLE `police_type`
  ADD PRIMARY KEY (`police_type_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `JMBG` (`jmbg`);

--
-- Indexes for table `user_police`
--
ALTER TABLE `user_police`
  ADD PRIMARY KEY (`user_police_id`),
  ADD KEY `FK_user_police_police_type` (`police_type_id`),
  ADD KEY `FK_user_police_user` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `police_assets`
--
ALTER TABLE `police_assets`
  MODIFY `police_assets_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `police_meta`
--
ALTER TABLE `police_meta`
  MODIFY `police_meta_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `police_type`
--
ALTER TABLE `police_type`
  MODIFY `police_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `user_police`
--
ALTER TABLE `user_police`
  MODIFY `user_police_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `police_assets`
--
ALTER TABLE `police_assets`
  ADD CONSTRAINT `FK_police_assets_police_meta` FOREIGN KEY (`police_meta_id`) REFERENCES `police_meta` (`police_meta_id`),
  ADD CONSTRAINT `FK_police_assets_police_type` FOREIGN KEY (`police_type_id`) REFERENCES `police_type` (`police_type_id`),
  ADD CONSTRAINT `FK_police_assets_user_police` FOREIGN KEY (`user_police_id`) REFERENCES `user_police` (`user_police_id`) ON DELETE CASCADE;

--
-- Constraints for table `police_meta`
--
ALTER TABLE `police_meta`
  ADD CONSTRAINT `FK_police_meta_police_type` FOREIGN KEY (`police_type_id`) REFERENCES `police_type` (`police_type_id`);

--
-- Constraints for table `user_police`
--
ALTER TABLE `user_police`
  ADD CONSTRAINT `FK_user_police_police_type` FOREIGN KEY (`police_type_id`) REFERENCES `police_type` (`police_type_id`),
  ADD CONSTRAINT `FK_user_police_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
