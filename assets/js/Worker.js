function changePoliceTipe() {
    var tip_polise = $('#tip_polise > option:selected').val();

    if (tip_polise == '') {
        $('#autoOdgovornosti').hide();
        $('#osiguranjeNepokretneImovineOdPozara').hide();
        $('#putnoOsiguranje').hide();
        $('#osiguranjeOdPosledicAnesrecnihSlucajeva').hide();
        $('#osiguranjeUsevaIPlodova').hide();
    } else if (tip_polise == 6) {
        $('#autoOdgovornosti').removeAttr("style");
        $('#osiguranjeNepokretneImovineOdPozara').hide();
        $('#putnoOsiguranje').hide();
        $('#osiguranjeOdPosledicAnesrecnihSlucajeva').hide();
        $('#osiguranjeUsevaIPlodova').hide();
    } else if (tip_polise == 7) {
        $('#autoOdgovornosti').hide();
        $('#osiguranjeNepokretneImovineOdPozara').removeAttr("style");
        $('#putnoOsiguranje').hide();
        $('#osiguranjeOdPosledicAnesrecnihSlucajeva').hide();
        $('#osiguranjeUsevaIPlodova').hide();
    } else if (tip_polise == 8) {
        $('#autoOdgovornosti').hide();
        $('#osiguranjeNepokretneImovineOdPozara').hide();
        $('#putnoOsiguranje').removeAttr("style");
        $('#nesrecnihSlucajeva').hide();
        $('#osiguranjeUsevaIPlodova').hide();
    } else if (tip_polise == 9) {
        $('#autoOdgovornosti').hide();
        $('#osiguranjeNepokretneImovineOdPozara').hide();
        $('#putnoOsiguranje').hide();
        $('#osiguranjeOdPosledicAnesrecnihSlucajeva').removeAttr("style");
        $('#osiguranjeUsevaIPlodova').hide();
    } else if (tip_polise == 10) {
        $('#autoOdgovornosti').hide();
        $('#osiguranjeNepokretneImovineOdPozara').hide();
        $('#putnoOsiguranje').hide();
        $('#osiguranjeOdPosledicAnesrecnihSlucajeva').hide();
        $('#osiguranjeUsevaIPlodova').removeAttr("style");
    }
}

$('select#izvoriFinansija').on('change', function () {

    var izvoriFinansija = $(this).val();

    if (izvoriFinansija == 0) {
        $('#group-nazivPrograma').hide();
    } else if (izvoriFinansija == 1) {
        $('#group-nazivPrograma').show();
    } else if (izvoriFinansija == 2) {
        $('#group-nazivPrograma').hide();
    } else if (izvoriFinansija == 3) {
        $('#group-nazivPrograma').hide();
    }
});

function init() {
    changePoliceTipe();
    validateAddPolice();
}

$(init);

//validacija za unos novih korisnika i izmenu postojacih
$(function () {
    $.validator.setDefaults({
        errorClass: 'help-block',
        highlight: function (element) {
            $(element)
                    .closest('.form-group')
                    .addClass('has-error');
        },
        unhighlight: function (element) {
            $(element)
                    .closest('.form-group')
                    .removeClass('has-error');
        }
    });

    $.validator.addMethod('strongPassword', function (value, element) {
        return this.optional(element)
                || value.length >= 6
                && /\d/.test(value)
                && /[a-z]/i.test(value);
    }, 'Lozinka mora da bude najmanje 6 karaktera i da sadrzi najmanje jedan broj!\'.')

    $('#user-form, #edit-user-form').validate({

        rules: {
            forname: {
                required: true,
                lettersonly: true
            },
            surname: {
                required: true,
                lettersonly: true
            },
            jmbg: {
                required: true,
                digits: true,
                minlength: 13,
                maxlength: 13
            },
            email: {
                required: true,
                email: true
            },
            tel: {
                required: true,
                digits: true,
                minlength: 5,
            },
            date: {
                required: true,
            },
            password: {
                required: true,
                strongPassword: true
            },
            ponovi: {
                required: true,
            }

        },
        messages: {
            forname: {
                required: 'Molimo vas unesite ime korisnika!',
                lettersonly: 'Molimo vas unesite validne podatke!'
            },
            surname: {
                required: 'Molimo vas unesite prezime korisnika!',
                lettersonly: 'Molimo vas unesite validne podatke!'
            },
            jmbg: {
                required: 'Molimo vas unesite JMBG korisnika!',
                digits: 'Molimo vas unesite validne podatke!',
                minlength: 'JMBG-a mora da bude duzi, 13 karaktera!',
                maxlength: 'JMBG-a mora da bude kraci, 13 karaktera!'
            },
            email: {
                required: 'Molimo vas unesite E-mail adresu korisnika!',
                email: 'E-mail adresa nije validna!'
            },
            tel: {
                required: 'Molimo vas unesite kontak telefon korisnika!',
                digits: 'Molimo vas unesite validne podatke!',
                minlength: 'Duzina mobilnog telefona ne moze da bude toliko kratka'
            },
            date: {
                required: 'Molimo vas unesite datum rodjenja korisnika!'
            },
            password: {
                required: 'Molimo vas unesite lozinku korisnika!'
            },
            ponovi: {
                required: 'Molimo vas ponovite lozinku korisnika!',
            }

        }
    });
});