<?php
    abstract class Controler {
        
        private $podaci = [];
        
        protected function set($name, $value){
            if(preg_match('/^[a-z]+[a-z_]*$/', $name)){
                $this->podaci[$name] = $value;
            }
        }
        
        final public function getData(){
            return $this->podaci;
        }

        public function index(){
            
        }
        
        public function convertforJs($name){
            if(substr_count($name, ' ') < 1 ){
                $name = strtolower($name);
                
                return $name;
                
            }elseif(substr_count($name, ' ') === 1 ){
                $part = explode(" ", $name);
                $part[0] = strtolower($part[0]);
                $second = ucfirst($part[1]);
                
                return $resolt = $part[0].''.$second;
                
            }elseif(substr_count($name, ' ') === 2){
                $part = explode(" ", $name);
                $part[0] = strtolower($part[0]);
                $second = ucfirst($part[1]);
                $third = ucfirst($part[2]);
                
                return $resolt = $part[0].''.$second.''.$third;
                
            }elseif(substr_count($name, ' ') === 3){
                $part = explode(" ", $name);
                $part[0] = strtolower($part[0]);
                $second = ucfirst($part[1]);
                $third = ucfirst($part[2]);
                $forth = ucfirst($part[3]);
                
                return $resolt = $part[0].''.$second.''.$third.''.$forth;
            }elseif(substr_count($name, ' ') === 4){
                $part = explode(" ", $name);
                $part[0] = strtolower($part[0]);
                $second = ucfirst($part[1]);
                $third = ucfirst($part[2]);
                $forth = ucfirst($part[3]);
                $fifth = ucfirst($part[4]);
                
                return $resolt = $part[0].''.$second.''.$third.''.$forth.''.$fifth;
            }    
    }
}
