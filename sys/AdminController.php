<?php
/**
 * Ova klasa prestavlja posebnu izvedenu klasu koja nasledjuje sve osobine
 * i funkcionalnosti osnovne klase kontrolea aplikacije, uz dopunu koja
 * prestavlja proveru postojanja prijavljenog korisnika (otvorena sesija za
 * korisnika) i ako to nije slucaj, preusmerava posetioca na logout rutu,
 * koja ce isprazniti sve iz sesije i presmeriti korisnika na login rutu.
*/
    class AdminController extends Controler{
        /**
         * Ovaj metod vrsi proveru postojanja prijavljenog korisnika(otvorena sesija
         * za korisnika). Ako korisnik koje prijavljen, preusmerava ga na lougout rutu,
         * koja ce isprazniti sve iz sesije i preusmeriti korisnika na login rutu.
         */
        final function __pre(){
            if(!Session::exists('user_id')){
                Misc::redirect('login');
            }
            
        }
    }
