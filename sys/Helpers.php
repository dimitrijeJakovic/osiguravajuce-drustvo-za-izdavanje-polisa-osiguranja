<?php
    class Helpers {
        
        public static function isUser(){
          if(Session::get('user_type') == 0){
              return true;
         }else{
            Session::end();
            Misc::redirect('login');
            return false;
         }
      }
      
      public static function isWorker(){
          if(Session::get('user_type') == 1){
              return true;
         }else{
            Session::end();
            Misc::redirect('login');
            return false;
         }
      }
      
    }
