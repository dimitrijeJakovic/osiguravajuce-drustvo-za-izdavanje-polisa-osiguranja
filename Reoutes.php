<?php
    return[
        [
            'Pattern'    => '|^login/?$|',
            'Controller' => 'Main',
            'Method'     => 'index'
        ],
        #worker routs
        [
            'Pattern'    => '|^worker/allUsers/?$|',
            'Controller' => 'Worker',
            'Method'     => 'allUsers'
        ],
        [
            'Pattern'    => '|^worker/allUsers/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'allUsers'
        ],
        [
            'Pattern'    => '|^worker/editUser/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'editUser'
        ],
        [
            'Pattern'    => '|^worker/deleteUser/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'deleteUser'
        ],
        [
            'Pattern'    => '|^worker/addUsers/?$|',
            'Controller' => 'Worker',
            'Method'     => 'addUsers'
        ],
        [
            'Pattern'    => '|^worker/ativation/?$|',
            'Controller' => 'Worker',
            'Method'     => 'ativation'
        ],
        [
            'Pattern'    => '|^worker/allPolices/?$|',
            'Controller' => 'Worker',
            'Method'     => 'allPolices'
        ],
        [
            'Pattern'    => '|^worker/allPolices/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'allPolices'
        ],
        [
            'Pattern'    => '|^worker/createPolice/?$|',
            'Controller' => 'Worker',
            'Method'     => 'createPolice'
        ],
        [
            'Pattern'    => '|^worker/createPolice/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'createPolice'
        ],
        [
            'Pattern'    => '|^worker/printPolice/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'printPolice'
        ],
        [
            'Pattern'    => '|^worker/editPolice/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'editPolice'
        ],
        [
            'Pattern'    => '|^worker/deletePolice/([0-9]+)/?$|',
            'Controller' => 'Worker',
            'Method'     => 'deletePolice'
        ],
        [
            'Pattern'    => '|^worker/?$|',
            'Controller' => 'Worker',
            'Method'     => 'index'
        ],
        [
            'Pattern'    => '|^logout/?$|',
            'Controller' => 'Worker',
            'Method'     => 'logout'
        ],
        #user routs
        [
            'Pattern'    => '|^user/?$|',
            'Controller' => 'User',
            'Method'     => 'index'
        ],
        [
            'Pattern'    => '|^user/edit/([0-9]+)/?$|',
            'Controller' => 'User',
            'Method'     => 'edit'
        ],
        [
            'Pattern'    => '|^user/editPass/([0-9]+)/?$|',
            'Controller' => 'User',
            'Method'     => 'editPass'
        ],
        [
            'Pattern'    => '|^user/printPolice/([0-9]+)/?$|',
            'Controller' => 'User',
            'Method'     => 'printPolice'
        ],
        [
            'Pattern'    => '|^logout/?$|',
            'Controller' => 'User',
            'Method'     => 'logout'
        ],
        #api routs
            #police types
        [
            'Pattern'    => '|^api/allPoliceTypes/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'allPoliceTypes'
        ],
        [
            'Pattern'    => '|^api/policeTypeById/([0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'policeTypeById'
        ],
        [
            'Pattern'    => '|^api/searchPoliceType/([A-z 0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'searchPoliceType'
        ],
            #users
        [
            'Pattern'    => '|^api/allUsers/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'allUsers'
        ],
        [
            'Pattern'    => '|^api/userById/([0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'userById'
        ],
        [
            'Pattern'    => '|^api/searchUser/([A-z 0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'searchUser'
        ],
            #user polices
        [
            'Pattern'    => '|^api/allUserPolises/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'allUserPolises'
        ],
        [
            'Pattern'    => '|^api/userPoliceById/([0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'userPoliceById'
        ],
        [
            'Pattern'    => '|^api/searchUserPolises/([A-z 0-9]+)/?$|',
            'Controller' => 'ApiWorker',
            'Method'     => 'searchUserPolises'
        ],
        [   #osnovna ruta
          'Pattern'    => '|^.*$|',
          'Controller' => 'Main',
          'Method'     => 'index' 
        ]
    ];
