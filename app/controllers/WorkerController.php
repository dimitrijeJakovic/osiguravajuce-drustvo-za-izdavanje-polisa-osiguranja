<?php

/**Ruta za ocitavanje paginacije*/
use Kilte\Pagination\Pagination;

/**
 * Ovaj kontroler aplikacije koji sluzi za zaposlene
 */
class WorkerController extends Controler {
    
    /**
    * Ovaje metod pre svega proverava da li se ulogovao zapolseni sa pristupom
    * na ovu stranicu. Ako jeste prikazuje panel za uvid svih korisnika
    * i polisa ako i mogucnosti izmene i prisanja
    */
    public function __construct() {
        Helpers::isWorker();
    }

    /**
     * Metod sluzi za ispisivanje na view ime i prezime zaposlenog.
     */
    function index() {
        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    /**
     * Metod ispisuje sve postojace korisnike na viewu.
     * U metodu postoji paginacija(10 korisnika po stranici).
     */
    function allUsers($currentPage = 1) {

        $totalUsers = UserModel::countUsers();
        Session::set('currentPage', $currentPage);

        $pagination = new Pagination($totalUsers->total, $currentPage, '10');
        $offset = $pagination->offset();
        $limit = $pagination->limit();

        $allUsers = UserModel::limitUsers($limit, $offset);

        $pages = $pagination->build();

        if (isset($_POST['submitSearch'])) {
            $resolt = filter_input(INPUT_POST, 'resolt');
            if ($resolt == '') {
                $this->set('message', 'Neispravno uneti podaci!');
            } else {
                $allUsers = UserModel::getUserJMBG($resolt);
                if (!$allUsers) {
                    Misc::redirect('worker/allUsers/');
                }
            }
        }


        $this->set('users', $allUsers);
        $this->set('pages', $pages);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function editUser($user_id) {
        if (isset($_POST['submit'])) {
            $forname = filter_input(INPUT_POST, 'forname');
            $surname = filter_input(INPUT_POST, 'surname');
            $jmbg = filter_input(INPUT_POST, 'jmbg');
            $email = filter_input(INPUT_POST, 'email');
            $tel = filter_input(INPUT_POST, 'tel');
            $date = filter_input(INPUT_POST, 'date');
            $pass = filter_input(INPUT_POST, 'password');

            if ($forname == '' or $surname == '' or $jmbg != preg_match('/^[0-9]{13}$/', $jmbg) or $email == '' or $tel != '' or $date == '' or $pass = '') {
                $this->set('message', 'Neispravno uneti podaci!');
            } else {
                $pass = hash('sha512', $pass . Configuration::SALT);

                $res = UserModel::editById($user_id, $forname, $surname, $jmbg, $email, $tel, $date, $pass);
                if ($res) {
                    Misc::redirect('worker/allUsers/');
                } else {
                    $this->set('message', 'Neuspesno promenjeni podaci!');
                }
            }
        }


        $user = UserModel::getById($user_id);
        $this->set('user', $user);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function deleteUser($user_id) {
        if ($_POST) {
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_NUMBER_INT);

            if ($confirmed == 1) {
                $res = UserModel::delateUser($user_id);
                if ($res) {
                    Misc::redirect('worker/allUsers');
                } else {
                    $this->set('message', 'Neuspesno izbrisan korisnik!');
                }
            }
        }

        $user = UserModel::getById($user_id);
        $this->set('user', $user);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function ativation() {
        if (isset($_POST['submit'])) {
            $user_id = filter_input(INPUT_POST, 'user_id');
            $active = filter_input(INPUT_POST, 'active', FILTER_SANITIZE_NUMBER_INT);

            $user = UserModel::getById($user_id);
            if ($user) {
                $res = UserModel::editActiveDeactive($active, $user_id);
                if ($res) {
                    Misc::redirect('worker/allUsers/' . Session::get('currentPage'));
                } else {
                    $this->set('message', 'Neuspesno promenjen status korisnika');
                }
            } else {
                $this->set('message', 'User nepostoji');
            }
        }

        $users = UserModel::getAllusers();
        $this->set('users', $users);
        Misc::redirect('worker/allUsers');
    }

    function addUsers() {
        if (isset($_POST['submit'])) {
            $forname = filter_input(INPUT_POST, 'forname');
            $surname = filter_input(INPUT_POST, 'surname');
            $jmbg = filter_input(INPUT_POST, 'jmbg');
            $email = filter_input(INPUT_POST, 'email');
            $tel = filter_input(INPUT_POST, 'tel');
            $date = filter_input(INPUT_POST, 'date');
            $password = filter_input(INPUT_POST, 'password');
            $ponovi = filter_input(INPUT_POST, 'ponovi');

            $userExists = UserModel::userExists($jmbg);
            
            $worker_id = Session::get('user_id');

            if ($forname != '' or $surname != '' or $jmbg != preg_match('/^[0-9]{13}$/', $jmbg) or $email != '' or $tel != '' or $date != '' or $password != '' or $ponovi != '' or $worker_id != '') {
                if ($userExists === 1) {
                    $this->set('message', 'Korisnik sa jedinstvenim maticnim brojem: ' . $jmbg . ' vec postoji!');
                } elseif ($userExists === 0) {
                    if ($password == $ponovi) {
                        $passwordHash = hash('sha512', $password . Configuration::SALT);
                        $res = UserModel::addUser($forname, $surname, $jmbg, $email, $tel, $date, $passwordHash, $worker_id);
                        if ($res) {
                            Misc::redirect('worker/allUsers/');
                        } else {
                            $this->set('message', 'Neuspesno kreiran korisnik!');
                        }
                    } else {
                        $this->set('message', 'Lozinke nisu iste!');
                    }
                }
            } else {
                $this->set('message', 'Sva polja moraju da budu popunjena!');
            }
        } else {
            
        }

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function allPolices($currentPagePolice = 1) {

        $totalPolises = UserPoliceModel::countPolises();
        Session::set('currentPagePolice', $currentPagePolice);

        $pagination = new Pagination($totalPolises->total, $currentPagePolice, '10');
        $offset = $pagination->offset();
        $limit = $pagination->limit();

        $allPolises = UserPoliceModel::limitPolises($limit, $offset);

        $pages = $pagination->build();

        if (isset($_POST['submitSearch'])) {
            $resolt = filter_input(INPUT_POST, 'resolt');
            if ($resolt == '') {
                $this->set('message', 'Neispravno uneti podaci!');
            } else {
                $allPolises = UserPoliceModel::searchUserPolises($resolt);
                if (!$allPolises) {
                    Misc::redirect('worker/allPolices/');
                }
            }
        }

        $this->set('police', $allPolises);
        $this->set('pages', $pages);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function createPolice() {
        
        if (isset($_POST['submit'])) {
            $user_id = filter_input(INPUT_POST, 'hiden', FILTER_SANITIZE_NUMBER_INT);
            
            if(!$user_id){
                $user_id = filter_input(INPUT_POST, 'korisnik', FILTER_SANITIZE_NUMBER_INT);
            }
            
            $police_type_id = filter_input(INPUT_POST, 'tip_polise');

            $start_date = filter_input(INPUT_POST, 'datumPocetkaP');
            $expired_date = filter_input(INPUT_POST, 'datumIstekaP');
            $value = filter_input(INPUT_POST, 'vredost');
            $tearms = filter_input(INPUT_POST, 'uslovi');
            $worker_id = Session::get('user_id');


            if ($user_id != '' or $police_type_id != '' or ! isset($start_date) or ! isset($expired_date) or $value != '' or $tearms != '' or $worker_id != '') {

                $resUserpolice = UserPoliceModel::addUserPolice($user_id, $police_type_id, $start_date, $expired_date, $value, $tearms, $worker_id);

                if ($resUserpolice > 0) {

                    foreach ($_POST as $key => $value) {
                        if (is_int($key)) {
                            $policeMeta[$key] = $value;
                        }
                    }

                    foreach ($policeMeta as $key => $value) {
                        if ($value != '') {
                            $userMetaInput = PoliceAssetsModel::addMeta($resUserpolice, $police_type_id, $key, $value);
                        }
                    }
                    
                    Misc::redirect('worker/allPolices/');
                } else {
                    $this->set('message', 'Neuspesno kreirana polisa!');
                }
            } else {
                $this->set('message', 'Sva polja moraju da budu popunjena!');
            }
        }
        
        if(isset($_POST['userForm'])){
            $userForm = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_NUMBER_INT);
            
            if(!$userForm == ''){
                $res = UserModel::getById($userForm);
                $this->set('user', $res);
               
            }
        }
        

        $users = UserModel::getAllusers();
        $this->set('users', $users);

        $policeType = PoliceTypeModel::getAll();
        $this->set('policetype', $policeType);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function printPolice($jmbg) {

        $polis = UserPoliceModel::getPoliceByUserJmbg($jmbg);
        $this->set('polis', $polis);
    }

    function editPolice($user_police_id) {

        $edit_police = UserPoliceModel::getMateByUserPolice($user_police_id);
        $this->set('edit_police', $edit_police);

        $user_police = UserPoliceModel::getById($user_police_id);
        $this->set('user_police', $user_police);
        
        if (isset($_POST['submit'])) {
            $start_date = filter_input(INPUT_POST, 'datumPocetkaP');
            $expired_date = filter_input(INPUT_POST, 'datumIstekaP');
            $value = filter_input(INPUT_POST, 'vredost');
            $tearms = filter_input(INPUT_POST, 'uslovi');
                        
            if (!isset($start_date) or !isset($expired_date) or $value != '' or $tearms != '') {
                
                $date = strtotime($start_date);
                $newSDate = date('Y/d/m', $date);
                
                $dateE = strtotime($expired_date);
                $newEDate = date('Y/d/m', $dateE);
                
                $resUserpolice = UserPoliceModel::editUserPolice($user_police_id, $newSDate, $newEDate, $value, $tearms);
                
                if($resUserpolice){
                    
                    foreach ($_POST as $key => $value) {
                        if (is_int($key)) {
                            $policeMeta[$key] = $value;
                        }
                    }
                    
                    foreach ($policeMeta as $key => $value) {
                        if ($value != '') {
                            $userMetaInput = PoliceAssetsModel::editMeta($user_police_id, $key, $value);
                        }
                    }
                    
                    Misc::redirect('worker/allPolices/');
                }else {
                    $this->set('message', 'Neuspesno kreirana polisa!');
                }
            }else {
                $this->set('message', 'Sva polja moraju da budu popunjena!');
            }
        }
        

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function deletePolice($user_police_id) {
        if ($_POST) {
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_NUMBER_INT);

            if ($confirmed == 1) {
                $res = UserPoliceModel::delatePolice($user_police_id);
                if ($res) {
                    Misc::redirect('worker/allPolices/');
                } else {
                    $this->set('message', 'Neuspesno izbrisan korisnik!');
                }
            }
        }

        $user_police = UserPoliceModel::getPoliceByUserPoliceId($user_police_id);
        $this->set('user_police', $user_police);

        $worker = UserModel::getById(Session::get('user_id'));
        $this->set('worker', $worker);
    }

    function logout() {
        Session::end();
        Misc::redirect('login');
    }

}
