<?php
/*
 * Ovo je osnovni kontroler aplikacije koji se koristi za izvrstvanje
 * zahteva upucenih prema podrazumevanim rutama koje poznaje veb sajt.
*/
    class MainController extends Controler{
        /**
         * Ovaj metod proverava da li postoje podaci za prijavu poslati HTTP POST
         * metodom, vrsi njihovu validaciju, proverava  postojanje korisnika sa tim
         * pristupnim parametrima i u slucaju da sve provere prodju bez greske
         * metod kreira sesiju za korisnika i na osnovu tipa korisnika preusmerava 
         * korisnika na deault rutu
         * @return void Metod ne vraca nista, vec koristi retun naredbu za prekid
         * izvrsavanja u odredjenim situacijama
         */
        function index() {
            if($_POST){
                $email = filter_input(INPUT_POST, 'email');
                $password = filter_input(INPUT_POST, 'password');
                
                if(preg_match('/^.{6,}$/', $email) and preg_match('/^.{6,}$/', $password)){
                    $passwordHash = hash('sha512', $password. Configuration::SALT);
                    $user = UserModel::getActiveUserByUserNameAndPasswordHashForUser($email, $passwordHash);
                    
                    if($user){
                        
                            $login = LoginStatusModel::addUserStatus($user->user_id);
                        
                            Session::set('user_id', $user->user_id);
                            Session::set('email', $email);
                            Session::set('user_type', $user->type);
                            Session::set('user_ip', $_SERVER['REMOTE_ADDR']);
                            Session::set('user_agent', $_SERVER['HTTP_USER_AGENT']);
                        if($user->type == 1){
                            Misc::redirect('worker');
                        }elseif($user->type == 0){
                            Misc::redirect('user');
                        }
                    } else {
                        $this->set('massage', 'Neispravan username ili password ili user nije aktivan!');
                    }
                } else {
                    $this->set('massage', 'Nepostojeci username ili password!');
                }
            }
        }
    }
