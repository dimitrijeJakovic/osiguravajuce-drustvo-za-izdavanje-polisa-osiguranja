<?php
/**
 * Ovaj kontroler aplikacije koji sluzi za korisnike
 */
    class UserController extends AdminController {
        /**
         * Ovaje metod pre svega proverava da li se ulogovala korsinik sa pristupom
         * na ovu stranicu. Ako jeste prikazuje informacije o korisniku i polise tog
         * korisnika, ako ne redirektujega na logout i brise sve iz sesije.
         */
        public function __construct() {
             Helpers::isUser();
        }
        /**
         * Metod koji salje view podatke o korisniku i sve polise koje su vezane za tog korisnika.
         */
        public function index() {
            $userPolice = UserPoliceModel::getPoliceByUserId(Session::get('user_id'));
            $this->set('police', $userPolice);

            $user = UserModel::getById(Session::get('user_id'));
            $this->set('users', $user);
        }
        /*
         * Metod koji uzima id polise i salje ga na view kao mi kasnije korisnik
         * mogao da istampa svoju polisu u koliko je to potrebno.
         * @param int $police_id
         */
        public function printPolice($police_id){
            $userPolicePrint = UserPoliceModel::policePrint(Session::get('user_id'), $police_id);

            $this->set('police_print', $userPolicePrint);
        }
        
        /**
         * Ovaj metod prikazuje formaular za izmenu podataka korsinika 
         * ili vrsi izmenu ako su podacui poslati HTTP POST metodom.
         */
        public function edit($user_id) {
            if (Session::get('user_id') == $user_id) {
                if (isset($_POST['submit'])) {
                    $forname = filter_input(INPUT_POST, 'forname');
                    $surname = filter_input(INPUT_POST, 'surname');
                    $jmbg = filter_input(INPUT_POST, 'jmbg');
                    $email = filter_input(INPUT_POST, 'email');
                    $tel = filter_input(INPUT_POST, 'tel');
                    $date = filter_input(INPUT_POST, 'date');

                    if ($forname == '' or $surname == '' or $jmbg == '' or $email == '' or $tel == '' or $date == '') {
                        $this->set('message', 'Neispravno uneti podaci!');
                    } else {
                        $res = UserModel::editById($user_id, $forname, $surname, $jmbg, $email, $tel, $date);
                        if ($res) {
                            Misc::redirect('user');
                        } else {
                            $this->set('message', 'Neuspesno promenjeni podaci!');
                        }
                    }
                }


                $user = UserModel::getById($user_id);
                $this->set('user', $user);
            } else {
                Session::end();
                Misc::redirect('login');
            }
            
            $userPolice = UserPoliceModel::getPoliceByUserId(Session::get('user_id'));
            $this->set('police', $userPolice);
        }
        
        /**
         * Ovaj metod prikazuje formaular za izmenu lozinke korisnika ili vrsi 
         * izmenu ako su podacui poslati HTTP POST metodom.
         */
        public function editPass($user_id) {
            if (Session::get('user_id') == $user_id) {
                if (isset($_POST['submit'])) {
                    $lozinka = filter_input(INPUT_POST, 'lozinka');
                    $ponovi = filter_input(INPUT_POST, 'ponovi');

                    if ($lozinka == '' or $ponovi == '') {
                        $this->set('message', 'Neispravno uneti podaci!');
                    } else {
                        if ($lozinka != $ponovi) {
                            $this->set('message', 'Lozinke nisu iste!');
                        } else {
                            $lozinka = hash('sha512', $lozinka . Configuration::SALT);
                            $res = UserModel::editPass($user_id, $lozinka);
                            if ($res) {
                                Misc::redirect('user');
                            } else {
                                $this->set('message', 'Neuspesno promenjena lozinka!');
                            }
                        }
                    }
                }
            } else {
                Session::end();
                Misc::redirect('login');
            }



            $user = UserModel::getById($user_id);
            $this->set('user', $user);
            
            $userPolice = UserPoliceModel::getPoliceByUserId(Session::get('user_id'));
            $this->set('police', $userPolice);
        }

        /**
         * Ovaj metod gasi sesiju cime efikasno unistava sve u sesiji,
         * a zatim preusmerava korisnika na login stranu.
         */
        function logout() {
            Session::end();
            Misc::redirect('login');
        }

    }
