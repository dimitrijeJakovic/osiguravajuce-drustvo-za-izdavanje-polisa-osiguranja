<?php
    class ApiWorkerController extends ApiController{
        
    public function __construct() {
        Helpers::isWorker();
    }
        
        public function allPoliceTypes(){
            $polises = PoliceTypeModel::getAll();
            if($polises){
                $this->set('polises', $polises);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine polises!');
            }
        }
        
        public function policeTypeById($id){
            $police_id = intval($id);
            $police = PoliceTypeModel::getById($police_id);
            if($police){
                $this->set('police', $police);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine police!');
            }
        }
        
        public function searchPoliceType($keyword){
            $police = PoliceTypeModel::searchPoliceType($keyword);
            if($police){
                $this->set('police', $police);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine police!');
            }
        }

        public function allUsers(){
            $users = UserModel::getAll();
            if($users){
                $this->set('users', $users);
                $this->set('status', 'succe');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine users!');
            }
        }
        
        public function userById($id){
            $user_id = intval($id);
            $user = UserModel::getById($user_id);
            if($user){
                $this->set('users', $user);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine user!');
            }
        }
        
        public function searchUser($keyword){
            $user = UserModel::searchUser($keyword);
            if($user){
                $this->set('users', $user);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine user!');
            }
        }

        public function allUserPolises(){
            $usersPolises = UserPoliceModel::getAll();
            if($usersPolises){
                $this->set('users', $usersPolises);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine users polises!');
            }
        }
        
        public function userPoliceById($id){
            $user_police_id = intval($id);
            $user_police = UserPoliceModel::getById($user_police_id);
            if($user_police){
                $this->set('users', $user_police);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine user police!');
            }
        }
        
        public function searchUserPolises($keyword){
            $user_police = UserPoliceModel::searchUserPolises($keyword);
            if($user_police){
                $this->set('users', $user_police);
                $this->set('status', 'succes');
            }else{
                $this->set('status', 'error');
                $this->set('message', 'Coud not fine user police!');
            }
        }
        
    }
