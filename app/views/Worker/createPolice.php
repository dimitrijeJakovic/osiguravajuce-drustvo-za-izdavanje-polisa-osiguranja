<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-6">
            <form id="police-form" method="post" class="form-horizontal formaEdit" id="createPolice">
                <?php if(!isset($_POST['userForm'])): ?>
                    
                        <div class="form-group">
                            <label class="control-label col-xs-2" for="korisnik">Izaberi korisnika:</label>
                            <div class="col-xs-10">
                                <select required id="korisnik" name="korisnik" class="form-control">
                                    <option value="">Izaberite korisnika</option>
                                    <?php foreach ($DATA['users'] as $user): ?>
                                        <option value="<?php echo $user->user_id; ?>"><?php echo $user->jmbg; ?> | <?php echo $user->forname;?> <?php echo $user->surname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                
                <?php else: ?>  
                
                    <div class="form-group">
                         <label for="korisnik" class="control-label col-sm-2">Korisnik:</label>
                         <div class="col-sm-10">
                             <input type="hidden" name="hiden" value="<?php echo $DATA['user']->user_id; ?>">
                             <input class="form-control" name="korisnik" type="text" id="korisnik" value="<?php echo $DATA['user']->forname; ?> <?php echo $DATA['user']->surname; ?> | <?php echo $DATA['user']->jmbg; ?>" disabled>
                         </div>
                     </div>
                   
                <?php endif; ?>
                <div class="form-group">
                    <label class="control-label col-xs-2" for="tip_polise">Tip polise:</label>
                    <div class="col-xs-10">
                        <select id="tip_polise" name="tip_polise" class="form-control"  onclick="return changePoliceTipe();" required>
                            <option value="">Izaberi polisu osiguranja</option>
                            <?php foreach ($DATA['policetype'] as $police): ?>
                                <option value="<?php echo $police->police_type_id ?>"><?php echo $police->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <?php foreach ($DATA['policetype'] as $police): ?>
                    <div id="<?php echo Controler::convertforJs($police->name); ?>">
                        <?php foreach (PoliceMetaModel::metaType($police->police_type_id) as $feald): ?>
                            <?php if ($feald->type == 'text' or $feald->type == 'date'): ?>
                                <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($feald->name); ?>">
                                    <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($feald->name); ?>"><?php echo $feald->name; ?>:</label>
                                    <div class="col-xs-10">
                                        <input type="<?php echo $feald->type; ?>" name="<?php echo $feald->police_meta_id; ?>" id="<?php echo Controler::convertforJs($feald->name); ?>" class="form-control">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($feald->type == 'textarea'): ?>
                                <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($feald->name); ?>">
                                    <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($feald->name); ?>"><?php echo $feald->name; ?>:</label>
                                    <div class="col-xs-10"> 
                                        <textarea name="<?php echo $feald->police_meta_id; ?>" class="form-control" id ="<?php echo Controler::convertforJs($feald->name); ?>" rows="5"></textarea>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($feald->type == 'dropdown'): ?>
                                <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($feald->name); ?>">
                                    <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($feald->name); ?>"><?php echo $feald->name; ?>:</label>
                                    <div class="col-xs-10"> 
                                        <select id="<?php echo Controler::convertforJs($feald->name); ?>" name="<?php echo $feald->police_meta_id; ?>" class="form-control">
                                            <option value="" selected>Izaberi izvor finansija</option>
                                            <option value="1">Supstencija drazave</option>
                                            <option value="2">Poljoprivredno drustvo</option>
                                            <option value="3">Poljoprivredno zadruga</option>
                                        </select>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="datumPocetkaP">Datum pocetka polise:</label>
                    <div class="col-xs-10">
                        <input type="date" name="datumPocetkaP" id="datumPocetkaP" class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="datumIstekaP">Datum isteka polise:</label>
                    <div class="col-xs-10">
                        <input type="date" name="datumIstekaP" id="datumIstekaP" class="form-control" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="vredost">Vrednost:</label>
                    <div class="col-xs-10">
                        <input type="number" step="0.01" name="vredost" id="vredost" class="form-control" placeholder="Unesite nominalnu vrednost polise" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="uslovi">Uslovi:</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" rows="5" id="uslovi" name="uslovi" placeholder="Unesite pod kojim uslovim vazi osiguranje" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <p class="btn btn-danger nazadbt nazadbt1 pull-right"><?php Misc::url('worker/allPolices', 'Nazad') ?></p>
                        <button type="submit" name="submit" class="btn btn-success pull-right">Napravi polisu</button>
                    </div>
                </div>

            </form>
            <?php if (isset($DATA['message'])): ?>
                <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>