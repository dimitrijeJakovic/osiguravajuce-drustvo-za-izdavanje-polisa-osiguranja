<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-sm-4">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-lg-9 col-sm-8">
            <p class="spisakK">Spisak svih polisa:</p>
            <div class="col-sm-6 col-sm-offset-2">
                <form method="post">
                    <div id="imaginary_container"> 
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control"  name="resolt" value="<?= isset($_POST['resolt']) ? $_POST['resolt'] : ''?>" placeholder="Pretraga po JMBG-u" >
                            <span class="input-group-addon">
                                <button type="submit" name="submitSearch" >
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>JMBG</th>
                        <th>Ime</th>
                        <th>Prezime</th>
                        <th>Tip polise</th>
                        <th>Pocetak vanjeza</th>
                        <th>Kraj vanjeza</th>
                        <th>Vrednost</th>
                        <th>Uslovi</th>
                        <th colspan="3">Opcije</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($DATA['police'] as $police): ?>
                        <tr>
                            <td><?php echo $police->jmbg; ?></td>
                            <td><?php echo $police->forname; ?></td>
                            <td><?php echo $police->surname; ?></td>
                            <td><?php echo $police->name; ?></td>
                            <td><?php echo date_format(date_create($police->start_date),"d/m/Y"); ?></td>
                            <td><?php echo date_format(date_create($police->expire_date),"d/m/Y"); ?></td>
                            <td><?php echo $police->value; ?>&euro;</td>
                            <td><?php echo substr($police->terms, 0, 40).'...'; ?></td>
                            
                            <td><a href="<?php echo Configuration::BASE; ?>worker/printPolice/<?php echo $police->jmbg; ?>"><i class="fa fa-print" aria-hidden="true"></i></a></td>
                            <td><a href="<?php echo Configuration::BASE; ?>worker/editPolice/<?php echo $police->user_police_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            <td><a href="<?php echo Configuration::BASE; ?>worker/deletePolice/<?php echo $police->user_police_id; ?>"><i class="fa fa fa-trash" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php if(!isset($_POST['resolt'])):?>
            <ul class="pagination pull-right">
                <?php foreach ($DATA['pages'] as $key => $page): ?>
                    <li class="<?= $page == 'current' ? 'active' : '' ?>"><a href="<?= Configuration::BASE . 'worker/allPolices/' . $key ?>"><?= $key ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php endif;?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>