<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-6">
            <form id="edit-user-form" class="form-horizontal formaEdit" method="POST" action="<?php echo Configuration::BASE; ?>worker/editUser/<?php echo $DATA['user']->user_id; ?>">
                <div class="form-group">
                    <label for="forname" class="control-label col-sm-2">Ime:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="forname" type="text" id="forname" value="<?php echo $DATA['user']->forname; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="surname" class="control-label col-sm-2">Prezime:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="surname" type="text" id="surname" value="<?php echo $DATA['user']->surname; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="JMBG" class="control-label col-sm-2">JMBG:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="jmbg" type="text" id="JMBG" value="<?php echo $DATA['user']->jmbg; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-sm-2">Email:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="email" type="text" id="email" value="<?php echo $DATA['user']->email; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tel" class="control-label col-sm-2">Telefon:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="tel" type="text" id="tel" value="<?php echo $DATA['user']->telfon_number; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="control-label col-sm-2">Datum rodjenja:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="date" type="date" id="date" value="<?php echo $DATA['user']->date_of_birth; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="control-label col-sm-2">Nova lozinka:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="password" type="text" id="pass">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-12 col-sm-offset-10">
                        <button type="submit" name="submit" class="btn btn-success">Sacuvaj</button>
                        <p class="btn btn-danger nazadbt"><?php Misc::url('worker/allUsers/', 'Nazad') ?></p>
                    </div>
                </div>
            </form>
            <?php if(isset($DATA['message'])): ?>
                <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>
