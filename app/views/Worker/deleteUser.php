<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-9">
            <header>
                <h3>Da li zelite da izbrisete korisnika: &quot; <?php echo htmlspecialchars($DATA['user']->forname); ?> <?php echo htmlspecialchars($DATA['user']->surname);?> &quot;?</h3>
            </header>
            
            <form method="post" action="<?php echo Configuration::BASE; ?>worker/deleteUser/<?php echo $DATA['user']->user_id; ?>">
                <input type="hidden" name="confirmed" value="1">
                <button type="submit" class="btn btn-danger">Izbrisi korisnika</button>
                <p class="btn btn-default"><?php Misc::url('worker/allUsers', 'Nazad') ?></p>
            </form>
            
            <?php if(isset($DATA['message'])): ?>
            <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>