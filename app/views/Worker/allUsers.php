<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-sm-4">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-lg-9 col-sm-8">
            <p class="spisakK">Spisak svih korisnika:</p>
            <div class="col-sm-6 col-sm-offset-2">
                <form method="post">
                    <div id="imaginary_container"> 
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control"  name="resolt" value="<?= isset($_POST['resolt']) ? $_POST['resolt'] : ''?>" placeholder="Pretraga po JMBG-u" >
                            <span class="input-group-addon">
                                <button type="submit" name="submitSearch" >
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>JMBG</th>
                        <th>Ime</th>
                        <th>Prezime</th>
                        <th>E-mail</th>
                        <th>Telegon</th>
                        <th>Status</th>
                        <th colspan="2">Opcije</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($DATA['users'] as $user): ?>
                        <tr>
                            <td><?php echo $user->user_id; ?></td>
                            <td><?php echo $user->jmbg; ?></td>
                            <td><?php echo $user->forname; ?></td>
                            <td><?php echo $user->surname; ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td><?php echo $user->telfon_number; ?></td>

                            <td>
                                 
								
                            <td><a href="<?php echo Configuration::BASE; ?>worker/editUser/<?php echo $user->user_id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                            <td><a href="<?php echo Configuration::BASE; ?>worker/deleteUser/<?php echo $user->user_id; ?>"><i class="fa fa fa-trash" aria-hidden="true"></i></a></td>
                            
                            <form method="post" action="<?php echo Configuration::BASE; ?>worker/createPolice/<?php echo $user->user_id; ?>">
                                <input type="hidden" name="user" value="<?php echo $user->user_id; ?>">
                                <td><button type="submit" name="userForm" class="btn-xs btn-primary" title='Napravi polisu'><i class="fa fa fa-plus" aria-hidden="true"></i></button></td>
                            </form>
                
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
            <?php if(!isset($_POST['resolt'])):?>
            <ul class="pagination pull-right">
                <?php foreach ($DATA['pages'] as $key => $page): ?>
                    <li class="<?= $page == 'current' ? 'active' : '' ?>"><a href="<?= Configuration::BASE . 'worker/allUsers/' . $key ?>"><?= $key ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php endif;?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>