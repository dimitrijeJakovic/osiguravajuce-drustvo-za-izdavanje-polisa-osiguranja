<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-6">
            <form id="user-form" class="form-horizontal formaEdit" method="POST" action="<?php echo Configuration::BASE; ?>worker/addUsers/">
                <div class="form-group">
                    <label for="forname" class="control-label col-sm-2">Ime:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="forname" type="text" id="forname" placeholder="Upisite ime" value="<?php echo (isset($_POST['forname']) ? $_POST['forname'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="surname" class="control-label col-sm-2">Prezime:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="surname" type="text" id="surname"  placeholder="Upisite prezime" value="<?php echo (isset($_POST['surname']) ? $_POST['surname'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="JMBG" class="control-label col-sm-2">JMBG:</label>
                    
                    <div class="col-sm-10">
                        <input class="form-control" name="jmbg" type="number" id="JMBG"  placeholder="Upisite jedinstven maticni broj" value="<?php echo (isset($_POST['jmbg']) ? $_POST['jmbg'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-sm-2">Email:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="email" type="email" id="email"  placeholder="Upisite email adresu" value="<?php echo (isset($_POST['email']) ? $_POST['email'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tel" class="control-label col-sm-2">Telefon:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="tel" type="text" id="tel"  placeholder="Upisite broj telefona Npr: 0601479990" value="<?php echo (isset($_POST['tel']) ? $_POST['tel'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="control-label col-sm-2">Datum rodjenja:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="date" type="date" id="date"  value="<?php echo (isset($_POST['date']) ? $_POST['date'] : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="control-label col-sm-2">Lozinka:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="password" type="password" id="pass"  placeholder="Upisite novu lozinku">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Ppass" class="control-label col-sm-2">Ponovi Lozinka:</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="ponovi" type="password" id="Ppass"  placeholder="Ponovite novu lozinku">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-sm-12 col-sm-offset-10">
                        <button type="submit" name="submit" class="btn btn-success">Sacuvaj</button>
                        <p class="btn btn-danger nazadbt"><?php Misc::url('worker/allUsers/', 'Nazad') ?></p>
                    </div>
                </div>
            </form>
            <?php if(isset($DATA['message'])): ?>
                <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>
