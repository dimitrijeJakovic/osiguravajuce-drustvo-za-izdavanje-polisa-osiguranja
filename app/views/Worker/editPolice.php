<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-6">
            <form class="form-horizontal formaEdit" method="POST">
                <?php foreach ($DATA['edit_police'] as $edit_police): ?>
                    <?php if($edit_police->meta->type == 'text' or $edit_police->meta->type == 'date'): ?>
                        <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($edit_police->meta->name); ?>">
                            <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($edit_police->meta->name); ?>"><?php echo $edit_police->meta->name; ?>:</label>
                            <div class="col-xs-10">
                                <input type="<?php echo $edit_police->meta->type; ?>" name="<?php echo $edit_police->police_meta_id; ?>" id="<?php echo Controler::convertforJs($edit_police->meta->name); ?>" class="form-control" value="<?php echo $edit_police->value; ?>">
                            </div>
                        </div>
                    <?php endif; ?>
                
                    <?php if($edit_police->meta->type == 'textarea'):?>
                        <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($edit_police->meta->name); ?>">
                            <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($edit_police->meta->name); ?>"><?php echo $edit_police->meta->name; ?>:</label>
                            <div class="col-xs-10"> 
                                <textarea name="<?php echo $edit_police->police_meta_id; ?>" class="form-control" id ="<?php echo Controler::convertforJs($edit_police->meta->name); ?>" rows="5"><?php echo $edit_police->value; ?></textarea>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if($edit_police->meta->type == 'dropdown'): ?>
                        <div class="form-group" id="<?php echo 'group-'.Controler::convertforJs($edit_police->meta->name); ?>">
                            <label class="control-label col-xs-2" for="<?php echo Controler::convertforJs($edit_police->meta->name); ?>"><?php echo $edit_police->meta->name; ?>:</label>
                            <div class="col-xs-10"> 
                                <select id="<?php echo Controler::convertforJs($edit_police->meta->name); ?>" name="<?php echo $edit_police->police_meta_id; ?>" class="form-control">
                                    <option value="" selected>Izaberi izvor finansija</option>
                                    <option value="1">Supstencija drazave</option>
                                    <option value="2">Poljoprivredno drustvo</option>
                                    <option value="3">Poljoprivredno zadruga</option>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                
                <?php endforeach; ?>
               
                <div class="form-group">
                    <label class="control-label col-xs-2" for="datumPocetkaP">Datum pocetka polise:</label>
                    <div class="col-xs-10">
                        <input type="text" name="datumPocetkaP" id="datumPocetkaP" class="form-control" value="<?php echo date_format(date_create($DATA['user_police']->start_date),"d/m/Y"); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="datumIstekaP">Datum isteka polise:</label>
                    <div class="col-xs-10">
                        <input type="text" name="datumIstekaP" id="datumIstekaP" class="date form-control" required value="<?php echo date_format(date_create($DATA['user_police']->expire_date),"d/m/Y"); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="vredost">Vrednost:</label>
                    <div class="col-xs-10">
                        <input type="number" step="0.01" name="vredost" id="vredost" class="form-control"  required value="<?php echo $DATA['user_police']->value; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-2" for="uslovi">Uslovi:</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" rows="5" id="uslovi" name="uslovi" required><?php echo $DATA['user_police']->terms; ?></textarea>
                    </div>
                </div>
                
                <div class="form-group ">
                    <div class="col-sm-12 col-sm-offset-10">
                        <button type="submit" name="submit" class="btn btn-success">Sacuvaj</button>
                        <p class="btn btn-danger nazadbt"><?php Misc::url('worker/allPolices/', 'Nazad') ?></p>
                    </div>
                </div>
            </form>
            <?php if(isset($DATA['message'])): ?>
                <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>