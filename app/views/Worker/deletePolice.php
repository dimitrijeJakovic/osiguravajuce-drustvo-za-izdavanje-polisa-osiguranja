<?php require_once 'app/views/_global/header.php'; ?>

<div  class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <?php require_once 'app/views/_global/saidMenu.php'; ?>
        </div>
        <div class="col-sm-9">
            <header>
                <h3>Da li zelite da izbrisete polisu: &quot; <?php echo $DATA['user_police']->name; ?>&quot; pod JMBG-om: &quot;<?php echo $DATA['user_police']->jmbg;?> &quot;?</h3>
            </header>
            
            <form method="post" action="<?php echo Configuration::BASE; ?>worker/deletePolice/<?php echo $DATA['user_police']->user_police_id; ?>">
                <input type="hidden" name="confirmed" value="1">
                <button type="submit" class="btn btn-danger">Izbrisi polisu</button>
                <p class="btn btn-default"><?php Misc::url('worker/allPolices/', 'Nazad') ?></p>    
            </form>
            
            <?php if(isset($DATA['message'])): ?>
            <p><?php echo htmlspecialchars($DATA['message']); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>