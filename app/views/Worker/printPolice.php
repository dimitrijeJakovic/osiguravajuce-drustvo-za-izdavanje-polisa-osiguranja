<?php

$html = '
    <div class="printPolice">
        <h2>Polisl: '. $DATA["polis"]->name .'</h2>
        <h4>Uslovi pod kojim vazi polisa:</h4>    
        <p>'. $DATA["polis"]->terms .'</p>
        <table>
            <tbody>
                <tr>  
                    <td>Datum pocetka vazenja polise:</td>
                    <td>'. $DATA["polis"]->start_date .'</td> 
                </tr>
                <tr>    
                    <td>Datum isteka vazenja polise:</td>
                    <td>'. $DATA["polis"]->	expire_date .'</td>       
                </tr>
                <tr>
                    <td>Nominalna vrsdnost polise:</td>
                    <td>'. $DATA["polis"]->value .'&euro;</td> 
                </tr>
            </tbody>
        </table>

        <h4>Informacije o korisniku:</h4>
        <table>
            <tbody>
                <tr>  
                    <td>Ime:</td>
                    <td>'. $DATA["polis"]->forname .'</td> 
                </tr>
                <tr>    
                    <td>Prezime:</td>
                    <td>'. $DATA["polis"]->surname .'</td>       
                </tr>
                <tr>    
                    <td>JMBG:</td>
                    <td>'. $DATA["polis"]->jmbg .'</td>       
                </tr>
                <tr>    
                    <td>Email::</td>
                    <td>'. $DATA["polis"]->email .'</td>       
                </tr>
                <tr>
                    <td>Mobilni telefon:</td>
                    <td>'. $DATA["polis"]->telfon_number .'</td> 
                </tr>
                <tr>
                    <td>Datum rodjenja:</td>
                    <td>'. $DATA["polis"]->date_of_birth .'</td> 
                </tr>
            </tbody>
        </table>



        <p>Potpis osiguranika</p>
        <p>__________________</p>

        <p>Potpis zaposlenog</p>
        <p>__________________</p>

    </div>
';


$mpdf = new mPDF('c','A4','','',20,15,48,25,10,10);
$mpdf->WriteHTML($html);
$mpdf->Output(); 
exit;
