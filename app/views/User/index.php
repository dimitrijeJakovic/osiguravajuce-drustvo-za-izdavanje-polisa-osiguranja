<?php require_once 'app/views/_global/header.php'; ?>

<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profil-sidebar">
                <div class="profil-user-pic">
                    <img src="<?php echo Configuration::BASE; ?>assets/img/man.png" alt="slika_korisnika" class="imgr-responsice img-circle">
                </div>
                <div class="profile-use-menu">
                    <div class="table-responsive responsiv-table">
                        <table class="table bio-table">
                            <tbody>
                                <tr>  
                                    <td>Ime:</td>
                                    <td><?php echo $DATA['users']->forname; ?></td> 
                                </tr>
                                <tr>    
                                    <td>Prezime:</td>
                                    <td><?php echo $DATA['users']->surname; ?></td>       
                                </tr>
                                <tr>    
                                    <td>JMBG:</td>
                                    <td><?php echo $DATA['users']->jmbg; ?></td>       
                                </tr>
                                <tr>    
                                    <td>Email::</td>
                                    <td><?php echo $DATA['users']->email; ?></td>       
                                </tr>
                                <tr>
                                    <td>Mobilni telefon:</td>
                                    <td><?php echo $DATA['users']->telfon_number; ?></td> 
                                </tr>
                                <tr>
                                    <td>Datum rodjenja:</td>
                                    <td><?php echo $DATA['users']->date_of_birth; ?></td> 
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="profile-user-buttons">
                    <a class="btn btn-sm u-btn dole" href="<?php echo Configuration::BASE; ?>user/edit/<?php echo $DATA['users']->user_id; ?>">Izmeni</a>
                    <a class="btn btn-sm u-btn dole" href="<?php echo Configuration::BASE; ?>user/editPass/<?php echo $DATA['users']->user_id; ?>">Izmeni lozinku</a>
                    <p class="btn btn-sm r-btn"><?php Misc::url('logout', 'Izloguj se') ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9 profil-content">
            <?php require_once 'app/views/_global/tableUser.php'; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>