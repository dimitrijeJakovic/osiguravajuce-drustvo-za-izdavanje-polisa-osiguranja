<?php require_once 'app/views/_global/header.php'; ?>

<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profil-sidebar">
                <div class="profil-user-pic">
                    <img src="<?php echo Configuration::BASE; ?>assets/img/man.png" alt="slika_korisnika" class="imgr-responsice img-circle">
                </div>
                <div class="profile-use-menu">
                    <form method="post" action="<?php echo Configuration::BASE; ?>user/editPass/<?php echo $DATA['user']->user_id; ?>">
                        <div class="form-group">
                            <label for="f1_pass">Nova Lozink::</label>
                            <input type="password" name="lozinka" class="form-control" id="f1_pass" placeholder="Unesite novu lozinku" required>
                        </div>
                        <div class="form-group">
                            <label for="f1_new">Ponovi Loziku:</label>
                            <input type="password" name="ponovi" class="form-control" id="f1_new" placeholder="Ponovite novu lozinku" required>
                        </div>
                        <div class="profile-user-buttons">
                            <button type="submit" name="submit" class="btn btn-sm sacuvaj u-btn">Sacuvaj</button>
                            <p class="btn btn-sm u-btn"><?php Misc::url('user', 'Nazad') ?></p>
                        </div>
                    </form>
                    <?php if(isset($DATA['message'])): ?>
                    <p><?php echo htmlspecialchars($DATA['message']); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 profil-content">
            <?php require_once 'app/views/_global/tableUser.php'; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>