<?php require_once 'app/views/_global/header.php'; ?>

<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profil-sidebar">
                <div class="profil-user-pic">
                    <img src="<?php echo Configuration::BASE; ?>assets/img/man.png" alt="slika_korisnika" class="imgr-responsice img-circle">
                </div>
                <div class="profile-use-menu">
                    <form method="post" action="<?php echo Configuration::BASE; ?>user/edit/<?php echo $DATA['user']->user_id; ?>">
                        <div class="form-group">
                            <label for="f1_ime">Ime:</label>
                            <input type="text" name="forname" class="form-control" id="f1_ime" value="<?php echo $DATA['user']->forname; ?>">
                        </div>
                        <div class="form-group">
                            <label for="f1_prezime">Prezime:</label>
                            <input type="text" name="surname" class="form-control" id="f1_prezime" value="<?php echo $DATA['user']->surname; ?>">
                        </div>
                        <div class="form-group">
                            <label for="f1_jmbg">JMBG:</label>
                            <input type="text" name="jmbg"  class="form-control" id="f1_jmbg" value="<?php echo $DATA['user']->jmbg; ?>">
                        </div>
                        <div class="form-group">
                            <label for="f1_email">Email:</label>
                            <input type="text" name="email" class="form-control" id="f1_email" value="<?php echo $DATA['user']->email; ?>">
                        </div>
                        <div class="form-group">
                            <label for="f1_tel">Telefon:</label>
                            <input type="text" name="tel" class="form-control" id="f1_tel" value="<?php echo $DATA['user']->telfon_number; ?>">
                        </div>
                        <div class="form-group">
                            <label for="f1_rodj">Datum rodjenja:</label>
                            <input type="date" name="date" class="form-control" id="f1_rodj" value="<?php echo $DATA['user']->date_of_birth; ?>">
                        </div>
                        <div class="profile-user-buttons">
                            <button type="submit" name="submit" class="btn btn-sm sacuvaj u-btn">Sacuvaj</button>
                            <p class="btn btn-sm u-btn"><?php Misc::url('user', 'Nazad') ?></p>
                        </div>
                    </form>
                    <?php if(isset($DATA['message'])): ?>
                    <p><?php echo htmlspecialchars($DATA['message']); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 profil-content">
            <?php require_once 'app/views/_global/tableUser.php'; ?>
        </div>
    </div>
</div>

<?php require_once 'app/views/_global/footer.php'; ?>