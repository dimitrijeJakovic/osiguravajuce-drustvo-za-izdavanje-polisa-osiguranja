<div class="nav-side-menu">
    <div class="brand">Zaposleni: <b><?php echo htmlspecialchars($DATA['worker']->forname); ?> <?php echo htmlspecialchars($DATA['worker']->surname); ?></b></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">

                <li  data-toggle="collapse" data-target="#users" class="collapsed active">
                  <a href=""><i class="fa fa-users fa-lg"></i> Korisnici <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="users">
                    <li><a href="<?php echo Configuration::BASE; ?>worker/allUsers/1">Svi korisnici</a></li>
                    <li><a href="<?php echo Configuration::BASE; ?>worker/addUsers/">Dodaj Korisnika</a></li>
                </ul>

                <li data-toggle="collapse" data-target="#polices" class="collapsed active">
                  <a href="#"><i class="fa fa-file-text fa-lg"></i> Polise <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="polices">
                    <li><a href="<?php echo Configuration::BASE; ?>worker/allPolices/">Sve Polise</a></li>
                    <li><a href="<?php echo Configuration::BASE; ?>worker/createPolice/">Napravi novu</a></li>
                </ul>
                <li>
                    <a href="<?php echo Configuration::BASE; ?>worker/logout/"><i class="fa fa-sign-out fa-lg text-danger"></i>Izloguj se</a>
                </li>
            </ul>
     </div>
</div>