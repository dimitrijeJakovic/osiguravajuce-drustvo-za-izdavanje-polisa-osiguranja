<h3>Vase Polise:</h3>
                <table class="table">    
                    <thead>
                        <tr>
                            <th>Vrsta:</th>
                            <th>Datum pocetka:</th>
                            <th>Datum isteka:</th>
                            <th>Vrednost:</th>
                            <th>Uslovi:</th>
                            <th>Stampaj:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($DATA['police'] as $police): ?>
                        <tr>
                            <td><?php echo $police->name; ?></td>
                            <td><?php echo date_format(date_create($police->start_date),"d/m/Y"); ?></td>
                            <td><?php echo date_format(date_create( $police->expire_date),"d/m/Y"); ?></td>
                            <td><?php echo $police->value; ?>&euro;</td>
                            <td><?php echo substr($police->terms, 0, 25).'...'; ?></td>
                            <td><a href="<?php echo Configuration::BASE; ?>user/printPolice/<?php echo $police->user_police_id; ?>"><i class="fa fa-print" aria-hidden="true"></i></a></td>
                        </tr>
                         <?php endforeach; ?>
                    </tbody>
                </table>