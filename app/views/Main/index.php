<?php require_once 'app/views/_global/header.php'; ?>

<article>
    <div class="container ">
        <div class="row poz">
            <div class="col-md-12 ">
                <div class="wrap">
                    <header>
                        <p class="form-title">Log in</p>
                    </header>
                    <form method="post" class="login">
                        <div>
                            <label for="f1_email"></label>
                            <input type="text" name="email" id="f1_email" required placeholder="Email" pattern="^.{6,}$"/>
                        </div>
                        <div>
                            <label for="f1_password"></label>
                            <input type="password" name="password" id="f1_password" required placeholder="Password" pattern="^.{3,}$"/>
                        </div>

                        <input type="submit" value="Sign In" class="btn btn-success btn-sm" />
                    </form>
                    <?php if (isset($DATA['massage'])): ?>
                    <p class="text-center" id="message"><br><?php echo htmlspecialchars($DATA['massage']); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</article>

<?php require_once 'app/views/_global/footer.php'; ?>

