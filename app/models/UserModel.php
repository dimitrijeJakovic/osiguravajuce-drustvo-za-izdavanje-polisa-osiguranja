<?php
/**
 * Metod koji otvara tabelu User
 * 
*/
    class UserModel implements ModelInterface{
        
        /**
         * Metod koji vraca spisak svih Usera
         * @return array
        */
        public static function getAll(){
            $SQL = 'SELECT * FROM user ORDER BY surname;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        } 
        
        /**
         * Metod koji vraca objekat sa podatkom Usera ciji je user_id je dat kao argument metoda
         * @param int $user_id
         * @return stdClass|NULL
        */
        public static function getById($user_id){
            $user_id = intval($user_id);
            $SQL = 'SELECT * FROM user WHERE user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca objekat sa podacima aktivnog Usera ciji je email i passwordHash je dat kao argument metoda
         * @param string $email
         * @param string $passwordHash
         * @return stdClass|NULL
        */
        public static function getActiveUserByUserNameAndPasswordHashForUser($email, $passwordHash){
            $SQL = 'SELECT * FROM `user` WHERE `email` = ? AND `password` = ? AND `active` = 1';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$email, $passwordHash]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vrsi izmenu zapisa u tabele user po user_id-u
         * @param int $user_id
         * @param string $forname
         * @param string $surname
         * @param number $jmbg
         * @param text $email
         * @param text $tel
         * @param date $date
         * @param text $pass
         * @return boolean
         */
        public static function editById($user_id, $forname, $surname, $jmbg, $email, $tel, $date, $pass){
            $user_id = intval($user_id);
            $SQL = 'UPDATE user SET email = ?, forname = ?, surname = ?, jmbg = ?, telfon_number = ?, date_of_birth = ?, password = ?, active = 1, type = 0 WHERE user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$email, $forname, $surname, $jmbg, $tel, $date, $pass, $user_id]);
        }
        
        /**
         * Metod koji vrsi izmenu lozinke usera u tabele user po user_id-u
         * @param int $user_id
         * @param text $lozinka
         * @return boolean
         */
        public static function editPass($user_id, $lozinka){
            $user_id = intval($user_id);
            $SQL = 'UPDATE user SET password = ?, active = 1, type = 0 WHERE user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$lozinka, $user_id]);
        }
        
        /**
         * Metod koji vraca spisak svih Usera koji su tipa 0(0-user, 1-worker)
         * @return array
        */
        public static function getAllusers(){
            $SQL = 'SELECT * FROM user WHERE type = 0;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vrsi brisanje iz tabele user po user_id-u
         * @param int $user_id
         * @return boolean
         */
        public static function delateUser($user_id){
            $user_id = intval($user_id);
            $SQL = 'DELETE FROM user WHERE user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$user_id]);
        }
        
        /**
         * Metod koji vrsi izmenu aktivnosti korisnika iz tabele user po user_id-u
         * @param int $user_id
         * @param boolean $active
         * @return boolean
         */
        public static function editActiveDeactive($active, $user_id ){
            $user_id = intval($user_id);
            $SQL = 'UPDATE user SET active = ? WHERE user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$active, $user_id]);
        }
        
        /**
         * Metod koji vrsi dodavanje zapisa u tabelu user u bazu podataka
         * @param string $forname
         * @param string $surname
         * @param int $jmbg
         * @param text $email
         * @param number $tel
         * @param date $date
         * @param text $passwordHash
         * @return boolean
         */
        public static function addUser($forname, $surname, $jmbg, $email, $tel, $date, $passwordHash, $worke_id){
            $SQL = 'INSERT INTO user(email, password, forname, surname, jmbg, telfon_number, date_of_birth, worker_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$email, $passwordHash, $forname, $surname, $jmbg, $tel, $date, $worke_id]);
        }
        
        /**
         * Metod koji broiji spisak svih Usera ciji je tip 0(0-user, 1-worker)
         * @return array
        */
        public static function countUsers(){
            $SQL = 'SELECT COUNT(user_id) as total FROM user WHERE type = 0 AND worker_id = '.Session::get('user_id').';';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca niz objekata sa podatkom Usera ciji je limit i offset je dat kao argument metoda
         * @param int $limit
         * @param int $offset
         * @return stdClass|NULL
        */
        public static function limitUsers($limit, $offset){
            $SQL = 'SELECT * FROM user WHERE worker_id = '.Session::get('user_id').' LIMIT '.$limit.'  OFFSET '.$offset. ';';
            $prep = DataBase::getInstance();
            $res = $prep->query($SQL);
            $data = $res->fetchAll(PDO::FETCH_CLASS, get_called_class());
            return $data;
        }
        /**
         * Metod koji porverava da li User postoji u bazi i vraca true ili false
         * @param int $limit
         * @param int $offset
         * @return boolean
        */
        public static function userExists($jmbg){
            $sql = 'SELECT COUNT(*) from user WHERE jmbg = ? LIMIT 1';
            $data = DataBase::getInstance()->prepare($sql);
            $data->execute([$jmbg]);

            if($data->fetchColumn()){
                return 1; // found
                
            }else{
                return 0; // not found
            }
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User ciji je jmbg je dat kao argument metoda
         * metod moze da uzme deo jmbg ne mora da budeo ceo i vratice sve jmbg koje pocinju sa tom vrednoscu
         * @param int $jmbg
         * @return stdClass|NULL
        */
        public static function getUserJMBG($jmbg){
            $SQL = 'SELECT * FROM user WHERE jmbg  LIKE ? AND worker_id  = '.Session::get('user_id').' ';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute(["%".$jmbg."%"]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod vrsi pretragu i vraca niz objekat sa podatkom Usera ciji je keyword je dat kao argument metoda
         * metod moze da uzme deo imena, prezimena ili jmbga koji ne mora da budeo ceo i vratice sva imena, prezimena ili jmbg koje pocinju sa tom vrednoscu
         * @param int $keyword
         * @return stdClass|NULL
        */
        public static function searchUser($keyword){
            $SQL = 'SELECT * FROM user WHERE forname LIKE ? OR surname LIKE ? OR jmbg LIKE ? ORDER BY user_id';
            $prep = DataBase::getInstance()->prepare($SQL);
            $string = '%'.trim($keyword).'%';
            $prep->execute([$string, $string, $string]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
}
