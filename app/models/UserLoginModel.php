<?php

    class UserLoginModel implements ModelInterface{
        
    public static function getAll() {
        $SQL = 'SELECT * FROM user_login ORDER BY user_login_id;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public static function getByID($user_login_id) {
        $user_login_id = intval($user_login_id);
        $SQL = 'SELECT * FROM user_login WHERE user_login_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$user_login_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    
    public static function addUserLogin($user_id){
        $SQL = 'INSERT INTO user_login(user_id) VALUES (?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$user_id]);
    }

}
