<?php
/**
 * Metod koji otvara tabelu Police Meta
 * 
*/
    class PoliceMetaModel implements ModelInterface{
        
        /**
         * Metod koji vraca spisak svih Plice Meta poredjanih po id-u
         * @return array
        */
        public static function getAll(){
            $SQL = 'SELECT * FROM police_meta ORDER BY police_meta_id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        } 
        
        /**
         * Metod koji vraca objekat sa podacima Police Meta ciji je police_meta_id je dat kao argument metoda
         * @param int $police_meta_id
         * @return stdClass|NULL
        */
        public static function getById($police_meta_id){
            $police_meta_id = intval($police_meta_id);
            $SQL = 'SELECT * FROM police_meta WHERE police_meta_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$police_meta_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca niz sa podacima Police Meta ciji je police_type_id je dat kao argument metoda
         * @param int $police_type_id
         * @return array
        */
        public static function metaType($police_type_id){
            $SQL = 'SELECT * FROM police_meta WHERE police_type_id = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$police_type_id]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
    }
