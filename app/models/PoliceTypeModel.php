<?php
/**
 * Metod koji otvara tabelu Police Type
 * 
*/
    class PoliceTypeModel implements ModelInterface{
        /**
         * Metod koji vraca spisak svih Plice Type poredjanih po id-u
         * @return array
        */
        public static function getAll(){
            $SQL = 'SELECT * FROM police_type ORDER BY 	police_type_id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        } 
        
        /**
         * Metod koji vraca objekat sa podacima Police Type ciji je police_type_id je dat kao argument metoda
         * @param int $police_type_id
         * @return stdClass|NULL
        */
        public static function getById($police_type_id){
            $police_type_id = intval($police_type_id);
            $SQL = 'SELECT * FROM police_type WHERE police_type_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$police_type_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca niz sa podacima Police Type ciji je keyword je dat kao argument metoda
         * Medotu ne moda da bude prosledjena tacna vrednost, vratice niz svih objekata koji pocinju kao keyword
         * @param int $keyword
         * @return array
        */
        public static function searchPoliceType($keyword){
            $SQL = 'SELECT * FROM police_type WHERE name LIKE ? ORDER BY police_type_id';
            $prep = DataBase::getInstance()->prepare($SQL);
            $string = '%'.trim($keyword).'%';
            $prep->execute([$string]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

}
