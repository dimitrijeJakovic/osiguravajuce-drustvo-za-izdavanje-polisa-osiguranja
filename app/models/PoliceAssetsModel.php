<?php
/**
 * Metod koji otvara tabelu Police Assets
 * 
*/
    class PoliceAssetsModel implements ModelInterface {
        /**
         * Metod koji vraca spisak svih User Polisa poredjane po id-u
         * @return array
        */
        public static function getAll(){
            $SQL = 'SELECT * FROM police_assets ORDER BY police_assets_id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        } 
        
        /**
         * Metod koji vraca objekat sa podacima User Polise ciji je police_assets_id je dat kao argument metoda
         * @param int $police_assets_id
         * @return stdClass|NULL
        */
        public static function getById($police_assets_id){
            $police_assets_id = intval($police_assets_id);
            $SQL = 'SELECT * FROM police_assets WHERE police_assets_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$police_assets_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vrsi dodavanje zapisa u tabeli police_assets u bazu podataka
         * @param int $user_police_id
         * @param int $police_type_id
         * @param int $police_mera_id
         * @param string $value
         * @return boolean
         */
        public static function addMeta($user_police_id, $police_type_id, $police_mera_id, $value){
            $SQL = 'INSERT INTO police_assets(user_police_id, police_type_id, police_meta_id, value) VALUES (?, ?, ?, ?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$user_police_id, $police_type_id, $police_mera_id, $value]);
        }
        
        /**
         * Metod koji vrsi izmenu zapisa u tabeli police_assets u bazu podataka
         * @param int $user_police_id
         * @param int $key
         * @param string $value
         * @return boolean
         */
        public static function editMeta($user_police_id, $key, $value){
            $user_police_id = intval($user_police_id);
            $key = intval($key);
            $SQL = 'UPDATE police_assets SET value = ? WHERE user_police_id = ? AND police_meta_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$value, $user_police_id, $key]);
        }
}