<?php
/**
 * Metod koji otvara tabelu User Police
 * 
*/
    class UserPoliceModel implements ModelInterface{
        
        /**
         * Metod koji vraca spisak svih User Polisa sortirane po user_police_id
         * @return array
        */
        public static function getAll(){
            $SQL = 'SELECT * FROM user_police ORDER BY user_police_id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
            
        } 
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je user_police_id je dat kao argument metoda
         * @param int $user_police_id
         * @return stdClass|NULL
        */
        public static function getById($user_police_id){
            $user_police_id = intval($user_police_id);
            $SQL = 'SELECT * FROM user_police WHERE user_police_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_police_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je user_police_id je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: user i police_type
         * @param int $user_police_id
         * @return stdClass|NULL
        */
        public static function getPoliceByUserPoliceId($user_police_id){
            $user_police_id = intval($user_police_id);
            $SQL = 'SELECT * FROM user_police 
                    INNER JOIN user ON user_police.user_id = user.user_id 
                    INNER JOIN police_type ON user_police.police_type_id = police_type.police_type_id 
                    WHERE user_police.user_police_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_police_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je user_id je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: user i police_type
         * @param int $user_id
         * @return stdClass|NULL
        */
        public static function getPoliceByUserId($user_id){
            $user_id = intval($user_id);
            $SQL = 'SELECT * FROM user_police 
                    INNER JOIN user ON user_police.user_id = user.user_id 
                    INNER JOIN police_type ON user_police.police_type_id = police_type.police_type_id 
                    WHERE user.user_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_id]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je user_id i police_id je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: user i police_type
         * @param int $user_id
         * @param int $police_id
         * @return stdClass|NULL
        */
        public static function policePrint($user_id, $police_id){
            $user_id = intval($user_id);
            $SQL = 'SELECT * FROM user_police 
                    INNER JOIN user ON user_police.user_id = user.user_id 
                    INNER JOIN police_type ON user_police.police_type_id = police_type.police_type_id 
                    WHERE user.user_id = ? AND user_police.user_police_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_id, $police_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je jmbg je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: user i police_type
         * @param int $jmbg
         * @return stdClass|NULL
        */
        public static function getPoliceByUserJmbg($jmbg){
            $SQL = 'SELECT * FROM user_police 
                    INNER JOIN user ON user_police.user_id = user.user_id 
                    INNER JOIN police_type ON user_police.police_type_id = police_type.police_type_id 
                    WHERE user.jmbg = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$jmbg]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }

        /**
         * Metod koji vrsi dodavanje zapisa u tabelu user_police u bazu podataka
         * @param int $user_id
         * @param int $police_tyip_id
         * @param date $start_date
         * @param date $expired_date
         * @param text $value
         * @param text $tearms
         * @return boolean
         */
        public static function addUserPolice($user_id, $police_tyip_id, $start_date, $expired_date, $value, $tearms, $worker_id){
            $SQL = 'INSERT INTO user_police(user_id, police_type_id, start_date, expire_date, value, terms, worker_id) VALUES (?, ?, ?, ?, ?, ?, ?);';
            $db = DataBase::getInstance();
            $prep = $db->prepare($SQL);
            if($prep->execute([$user_id, $police_tyip_id, $start_date, $expired_date, $value, $tearms, $worker_id])) {
                return $db->lastInsertId();
            }
            return false;
        }
        
        /**
         * Metod koji broiji spisak svih User Polisa po user_police_id
         * @return array
        */
        public static function countPolises(){
            $SQL = 'SELECT COUNT(user_police_id) as total FROM user_police WHERE worker_id = '.Session::get('user_id').' ;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vraca niz objekata sa podatkom User Police ciji je limit i offset je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: user i police_type
         * @param int $limit
         * @param int $offset
         * @return stdClass|NULL
        */
        public static function limitPolises($limit, $offset){
            $SQL = 'SELECT * 
                   FROM user_police 
                   INNER JOIN user ON user_police.user_id = user.user_id 
                   INNER JOIN police_type ON user_police.police_type_id = police_type.police_type_id 
                   WHERE user_police.worker_id = '.Session::get('user_id').'
                   LIMIT '.$limit.'  OFFSET '.$offset. ';';
            $prep = DataBase::getInstance();
            $res = $prep->query($SQL);
            $data = $res->fetchAll(PDO::FETCH_CLASS, get_called_class());
            return $data;
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User ciji je jmbg je dat kao argument metoda
         * metod moze da uzme deo jmbg ne mora da budeo ceo i vratice sve jmbg koje pocinju sa tom vrednoscu
         * @param int $jmbg
         * @return stdClass|NULL
        */
        public static function getUserJMBG($jmbg){
            $SQL = 'SELECT * FROM user WHERE jmbg  LIKE ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute(["%".$jmbg."%"]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
        /**
         * Metod koji vrsi brisanje zapisa iz tabele user_police
         * @param int $user_police_id
         * @return boolean
         */
        public static function delatePolice($user_police_id){
            $user_police_id = intval($user_police_id);
            $SQL = 'DELETE FROM user_police WHERE user_police_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$user_police_id]);
        }
        
        /**
         * Metod koji vraca objekat sa podatkom User Police ciji je user_police_id je dat kao argument metoda
         * Inner Join-ovane su 2 tabele: police_assets i police_type
         * @param int $user_police_id
         * @return stdClass|NULL
        */
        public static function getMateByUserPolice($user_police_id){
            $user_police_id = intval($user_police_id);
            $SQL = 'SELECT *
                    FROM user_police
                    LEFT JOIN police_assets ON user_police.user_police_id = police_assets.user_police_id
                    LEFT JOIN police_type ON user_police.police_type_id = police_type.police_type_id
                    WHERE user_police.user_police_id = ?';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$user_police_id]);
            $res = $prep->fetchAll(PDO::FETCH_OBJ);
            
            foreach($res as $key=>$value){
                $res[$key]->meta = PoliceMetaModel::getById($res[$key]->police_meta_id);
            }
            return $res;
        }
        
        /**
         * Metod koji vrsi izmenu zapisa u tabele user_police po user_police_id-u
         * @param int $user_police_id
         * @param date $start_date
         * @param date $expired_date
         * @param text $value
         * @param text $tearms
         * @return boolean
         */
        public static function editUserPolice($user_police_id, $start_date, $expired_date, $value, $tearms){
            $user_police_id = intval($user_police_id);
            $SQL = 'UPDATE user_police SET start_date = ?, expire_date = ?, value = ?, terms = ?, active = 1 WHERE user_police_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$start_date, $expired_date, $value, $tearms, $user_police_id]);
        }
        
        /**
         * Metod vrsi pretragu i vraca niz objekat sa podatkom User Policea ciji je keyword je dat kao argument metoda
         * metod moze da uzme deo jmbg ne mora da budeo ceo i vratice sve jmbg koje pocinju sa tom vrednoscu
         * postoji left join sa tabelom user i inner join sa tabelom police_type
         * @param int $keyword
         * @return stdClass|NULL
        */
        public static function searchUserPolises($keyword){
            $SQL = 'SELECT * FROM `user_police` 
                    LEFT JOIN user on user_police.user_id = user.user_id 
                    INNER JOIN police_type on user_police.police_type_id = police_type.police_type_id 
                    WHERE user.jmbg LIKE ? AND user_police.worker_id = '.Session::get('user_id').' ;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $string = '%'.trim($keyword).'%';
            $prep->execute([$string]);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }
        
    }
