<?php
    class LoginStatusModel implements ModelInterface {
        public static function getAll() {
            $SQL = 'SELECT * FROM login_status ORDER BY login_status_id;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute();
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        public static function getByID($login_status_id) {
            $login_status_id = intval($login_status_id);
            $SQL = 'SELECT * FROM login_status WHERE login_status_id = ?;';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute([$login_status_id]);
            return $prep->fetch(PDO::FETCH_OBJ);
        }
        
        public static function addUserStatus($user_id){
            $user_id = intval($user_id);
            $SQL = 'INSERT INTO login_status(user_id) VALUES (?);';
            $prep = DataBase::getInstance()->prepare($SQL);
            return $prep->execute([$user_id]);
        }

}
